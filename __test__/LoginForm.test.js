import React from 'react';
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import LoginForm from '../main/components/LoginForm';
// и напишем наш тест для компонента

describe('Test LginForm component', () =>
{
    let wrapper
    const error = 10


    configure({ adapter: new Adapter() });
    beforeEach(() =>
    {
        wrapper = shallow(<LoginForm />)

    })

    it('render the component', () =>
    {
        expect(wrapper.length).toEqual(1)
    });

    it("check error message", () =>
    {
        const props = {
            error: 'test'
        },
            DateInputComponent = mount(<LoginForm {...props} />).find(
                ".red-color"
            );
        expect(DateInputComponent.hasClass("red-color")).toEqual(
            true
        );
    });

    it("check the onChange email", () =>
    {
        const props = {
            error: ''
        },
            DateInputComponent = mount(<LoginForm {...props} />);
        DateInputComponent.find("input").first().simulate("change", {
            target: { value: 'test@test.test', name: "email" }
        });
        DateInputComponent.update()
        expect(DateInputComponent.state().email.value).toEqual("test@test.test");
    });

    it("check the onChange password", () =>
    {
        const props = {
            error: ''
        },
            DateInputComponent = mount(<LoginForm {...props} />);
        DateInputComponent.find("input").last().simulate("change", {
            target: { value: 'test123', name: "password" }
        });
        DateInputComponent.update()
        expect(DateInputComponent.state().password.value).toEqual("test123");
    });

    it("check the onSubmit success", () =>
    {
        const onChange = jest.fn()
        const props = {
            error: '',
            login: onChange
        },
            DateInputComponent = mount(<LoginForm {...props} />);
        DateInputComponent.find("input").last().simulate("change", {
            target: { value: 'test123', name: "password" }
        });
        DateInputComponent.find("input").first().simulate("change", {
            target: { value: 'test@test.test', name: "email" }
        });
        DateInputComponent.update()
        DateInputComponent.find("form").simulate("submit");
        DateInputComponent.update()
        expect(onChange).toHaveBeenCalledTimes(1);
    });

    it("check the onSubmit error", () =>
    {
        const props = {
            error: '',
            login: () => {}
        },
            DateInputComponent = mount(<LoginForm {...props} />);
        DateInputComponent.find("input").last().simulate("change", {
            target: { value: 'test', name: "password" }
        });
        DateInputComponent.find("input").first().simulate("change", {
            target: { value: 'testest.test', name: "email" }
        });
        DateInputComponent.update()
        DateInputComponent.find("form").simulate("submit");
        DateInputComponent.update()
        expect(DateInputComponent.state().email.message).toEqual("invalid email");
    });

});