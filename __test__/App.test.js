import React from 'react';
import App from '../main/containers/App';
import renderer from 'react-test-renderer';
import { BrowserRouter as Router } from 'react-router-dom';

it('renders without crashing', () => {
  const TextInputComponent = renderer.create(<Router><App /></Router>).toJSON();
  expect(TextInputComponent).toMatchSnapshot();
}); 
