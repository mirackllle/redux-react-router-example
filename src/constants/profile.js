export const LOAD_PROFILE = 'LOAD_PROFILE'
export const PROFILE_SUCCESS = 'PROFILE_SUCCESS'
export const PROFILE_FAIL = 'PROFILE_FAIL'
export const PROFILE_USER_NOT_FOUND_MESSAGE = 'user not found, please login again'