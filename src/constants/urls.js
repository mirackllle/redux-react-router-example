export const host = 'https://mysterious-reef-29460.herokuapp.com/'
export const url_login = 'api/v1/validate'
export const url_profile_get = 'api/v1/user-info/'
export const url_news_get = 'api/v1/news'