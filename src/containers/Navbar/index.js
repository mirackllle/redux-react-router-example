import React from "react";
import Navbar from "../../components/Navbar";
import { connect } from "react-redux";
import { logout } from "../../actions/LoginActions";

class NavbarContainer extends React.Component {

  render() {
    const {auth, logout} = this.props
    return (
    <Navbar auth={auth} logout={logout} />
    )
  }
} 

const mapStateToProps = (state) => {
  return {
    auth: state.login
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logout()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavbarContainer)