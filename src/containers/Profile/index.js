import React from 'react';
import NavbarContainer from '../Navbar';
import Profile from '../../components/Profile';
import { connect } from 'react-redux';
import { getProfileData } from '../../actions/ProfileActions';

class ProfileContainer extends React.Component {
  render() {
    const {profileData, getProfileData} = this.props;
    return (
      <div>
        <NavbarContainer />
        <Profile profileData={profileData} getProfileData={getProfileData} />
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    profileData: store.profile.profileData
  }
}

const mapDispatchToProps = dispatch =>
{
  return {
    getProfileData: id => dispatch(getProfileData(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer)
