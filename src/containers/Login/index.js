import React from 'react';
import NavbarContainer from '../Navbar';
import { connect } from 'react-redux';
import LoginForm from '../../components/LoginForm';
import { login } from '../../actions/LoginActions';

class Login extends React.Component {

  render() {
    const {error, login} = this.props;
    return (
      <div>
        <NavbarContainer />
        <LoginForm login={login} error={error}/>
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    error: store.login.error
  }
}

const mapDispatchToProps = dispatch =>
{
  return {
    login: credentials => dispatch(login(credentials)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)