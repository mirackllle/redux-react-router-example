import React from 'react';
import NavbarContainer from '../Navbar';
import { getNews } from '../../actions/NewsActions';
import { connect } from 'react-redux';
import { News } from '../../components/News';

class NewsContainer extends React.Component {
  render() {
    const {newsData, getNews} = this.props
    return (
      <div> 
        <NavbarContainer />
        <News newsData={newsData} getNews={getNews} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    newsData: state.news.newsData,
    auth: state.login
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getNews: () => dispatch(getNews())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsContainer);