import React from 'react'
import { Route, Switch } from 'react-router-dom'

import App from './containers/App'
import Login from './containers/Login';
import News from './containers/News';
import NotFound from './containers/NotFound';
import ProfileContainer from './containers/Profile';
import requireAuthentication from './containers/AuthenticatedComponent';

export default class Routes extends React.Component
{
  render()
  {
    return <div>
      <Switch>
        <Route exact path='/' component={App} />
        <Route path='/profile' component={requireAuthentication(ProfileContainer)} />
        <Route path='/login' component={Login} />
        <Route path='/news' component={News} />
        <Route component={NotFound} />
      </Switch>
    </div>
  }
}