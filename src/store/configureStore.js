import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import { rootReducer } from '../reducers';
import { redirect } from './redirect';

export default function configureStore() {
  const store = createStore(rootReducer, applyMiddleware( thunk, redirect));

  return store
}