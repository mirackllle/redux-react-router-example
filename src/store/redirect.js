import { ROUTING } from '../constants/Routing'
import history from '../history'

export const redirect = store => next => action => { 
  if (action.type === ROUTING) {
    history.push(action.payload.nextUrl);
  }

  return next(action)
}