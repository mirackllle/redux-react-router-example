class Token {

  isAuthenticated() {
    if(window.sessionStorage.getItem('jwtToken')) {
      return true
    }
    if(window.localStorage.getItem('jwtToken')) {
      return true
    }
    return false
  }
  getJwtToken() {
    if(window.sessionStorage.getItem('jwtToken')) {
      return JSON.parse(window.sessionStorage.getItem('jwtToken'))
    }
    if(window.localStorage.getItem('jwtToken')) {
      return JSON.parse(window.localStorage.getItem('jwtToken'))
    }
    return null
  }
  setJwtToken(jwtToken, rememberMe) {
    if(rememberMe) {
      window.localStorage.setItem('jwtToken', JSON.stringify(jwtToken))
      return
    }
    window.sessionStorage.setItem('jwtToken', JSON.stringify(jwtToken))
  }
  logout() {
    window.localStorage.clear()
    window.sessionStorage.clear()
  }
}

export default new Token()