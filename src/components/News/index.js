import React from 'react'
import { Row, Col, CardPanel } from 'react-materialize'

export class News extends React.Component
{

  constructor(props) {
    super(props)
    props.getNews();
  }

  render()
  {
    let newsData = []
    console.log(this.props.newsData)
    if (this.props.newsData !== [])
    {
      newsData = this.props.newsData.map((elem) =>
      {
        return <Row key={elem.id}>
          <Col s={12} m={5}>
            <CardPanel className="teal lighten-4 black-text">
              <span>{elem.title}</span>
            </CardPanel>
          </Col>
          <Col s={12} m={7}>
            {elem.text}
          </Col>
        </Row>
      })
    }
    return (
      <div>
        {newsData}
      </div>
    )
  }
}