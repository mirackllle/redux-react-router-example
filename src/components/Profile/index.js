import React from 'react'
import {Collection, CollectionItem} from 'react-materialize'
import token from '../../utils/token';

export default class Profile extends React.Component {

  profileData;

  constructor (props) {
    super(props)
    props.getProfileData(token.getJwtToken().id)
  }

  render() {
    this.profileData = this.props.profileData
    let langs = []
    let links = []
    if(this.profileData.languages) {
      langs = this.profileData.languages.map((elem, index) => {
        return <CollectionItem key={index}>{elem}</CollectionItem>
      })
    }
    if(this.profileData.social) {
      links = this.profileData.social.map((elem, index) => {
        return <CollectionItem key={index} href={elem.link}>{elem.label}</CollectionItem>
      })
    }
    return (
      <div>
        <h4>City</h4>
        <Collection>
          <CollectionItem>{this.profileData.city}</CollectionItem>
        </Collection>
        <h4>Languages</h4>
        <Collection>
          {langs}
        </Collection>
        <h4>Socials</h4>
        <Collection>
          {links}
        </Collection>
      </div>
    );
  }
}