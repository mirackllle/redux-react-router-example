import React from 'react';
import { Link } from 'react-router-dom';

class Navbar extends React.Component
{
  render()
  {
    return (
      <div>
        <nav>
          <div className="nav-wrapper">
            <Link className="brand-logo" to='/'>Logo</Link>
            <ul id="nav-mobile" className="right hide-on-med-and-down">
              <li><Link to='/news'>News</Link></li>
              <li><Link to='/profile'>Profile</Link></li>
              {(() => { if(this.props.auth.isAuthenticated) return <li><Link to='/login' onClick={this.props.logout}>Logout</Link></li> })()}
              {(() => { if(!this.props.auth.isAuthenticated) return <li><Link to='/login'>Login</Link></li> })()}
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;