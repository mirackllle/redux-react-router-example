import React from 'react';
import './styles.css'
import { Input, Row, Button } from 'react-materialize';

export default class LoginForm extends React.Component {

  validation = {
    email: {
      pattern: /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i,
      message: 'invalid email'
    },
    password: {
      minS: 5,
      message: 'password is too short'
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      email: {
        value: '',
        message: ''
      },
      password: {
        value: '',
        message: ''
      }
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  render () {
    return (
      <form onSubmit={this.handleSubmit} className="input-field" >
        {(() => { if(this.props.error) return <p className="red-color">{this.props.error}</p> })()}
        {(() => { if(this.state.email.message) return <p className="red-color">{this.state.email.message}</p> })()}
        <Row><Input s={12} label="Email" value={this.state.email.value} onChange={this.handleChange} name="email"/></Row>
        {(() => { if(this.state.password.message) return <p className="red-color">{this.state.password.message}</p> })()}
        <Row><Input s={12} label="Password" value={this.state.password.value} onChange={this.handleChange} name="password" type="password"/></Row>
        <Row><Button className='btn-position' type="submit">Submit</Button></Row>
      </form>
    ); 
  }

  handleChange(e) {
    const name = e.target.name;
    const field = {
      value: e.target.value,
      message: ""
    }
    this.setState({[name]: field});
  }

  handleSubmit(event) {
    event.preventDefault();
    this.validateFields();
  }

  validateFields() {
    this.validation.email.isValid = false;
    this.validation.password.isValid = false;
    this.setState({email: {
      value: this.state.email.value,
      message: this.validField("email", this.state.email.value)
    }, 
      password: {
        value: this.state.password.value,
        message: this.validField("password", this.state.password.value)
      }}, () => {
      if(!this.state.email.message && !this.state.password.message) {
        this.props.login(
          {
            email: this.state.email.value,
            password: this.state.password.value
          });
      };
    });
  }

  validField(fieldName, value) {
    if(this.validation[fieldName].pattern) if(!value.match(this.validation[fieldName].pattern)) return this.validation[fieldName].message;
    if(this.validation[fieldName].minS) if(!(value.length >= this.validation[fieldName].minS)) return this.validation[fieldName].message;
    
  }
}
