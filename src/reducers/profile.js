import { LOAD_PROFILE, PROFILE_SUCCESS, PROFILE_FAIL } from "../constants/profile";

const initialState = {
  profileData: {}
}

export const profileReducer = (state = initialState, action) => {
  switch (action.type)
  {
    case LOAD_PROFILE:
      return { ...state}
    case PROFILE_SUCCESS:
      return {...state, profileData: action.payload}
    case PROFILE_FAIL:
      return{...state, error: action.payload.error}
    default:
      return state
  }
}