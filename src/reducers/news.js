import { NEWS_REQUEST, NEWS_FAIL, NEWS_SUCCESS } from "../constants/news";

const initialState = {
  newsData: []
}
export const newsReducer = (state = initialState, action) => {
  switch (action.type)
  {
    case NEWS_REQUEST:
      return { ...state}
    case NEWS_SUCCESS:
      return {...state, newsData: action.payload}
    case NEWS_FAIL:
      return{...state, error: action.payload.error}
    default:
      return state
  }
}