import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT } from "../constants/login";

const initialState = {
  token: 2018,
  isAuthenticated: false,
  error: ''
}
export const loginReducer = (state = initialState, action) =>
{
  switch (action.type)
  {
    case LOGIN_REQUEST:
      return { ...state}
    case LOGIN_SUCCESS:
      return {...state, token: action.payload.token, isAuthenticated: true, error: action.payload.error}
    case LOGIN_FAIL:
      return{...state, error: action.payload.error}
    case LOGOUT:
      return{...state, isAuthenticated: false}
    default:
      return state
  }
}