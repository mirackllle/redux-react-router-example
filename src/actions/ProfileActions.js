import { LOAD_PROFILE, PROFILE_SUCCESS, PROFILE_FAIL, PROFILE_USER_NOT_FOUND_MESSAGE } from "../constants/profile";
import axios from 'axios';
import { host, url_profile_get } from "../constants/urls";
import { SERVER_ERROR, SERVER_ERROR_MESASAGE, STATUS_OK } from "../constants/common";

export const getProfileData = (id) =>
{
  return async (dispatch) =>
  {
    dispatch({
      type: LOAD_PROFILE
    })
    const resp = await axios.get(`${host}${url_profile_get}${id}`)
    try {
      if (resp.data.status === STATUS_OK)
      {
        dispatch({
          type: PROFILE_SUCCESS,
          payload: resp.data.data
        })
      } else  {
        dispatch({
          type: PROFILE_FAIL,
          payload: PROFILE_USER_NOT_FOUND_MESSAGE
        })
      }
    } catch {
      dispatch({
        type: SERVER_ERROR,
        payload: SERVER_ERROR_MESASAGE
      })
    }
    
  }
}