import { ROUTING } from "../constants/Routing";
import axios from 'axios';
import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAIL, LOGIN_FAIL_MESSAGE, LOGOUT } from "../constants/login";
import { host, url_login } from "../constants/urls";
import token from "../utils/token";
import { STATUS_OK, SERVER_ERROR, SERVER_ERROR_MESASAGE } from "../constants/common";



export const login = (credentials) =>
{
  return async (dispatch) =>
  {
    dispatch({
      type: LOGIN_REQUEST
    })
    const resp = await axios.post(`${host}${url_login}`, credentials)
    try {
      if(resp.data.status === STATUS_OK) {
        token.setJwtToken(resp.data.data);
        dispatch({
          type: LOGIN_SUCCESS,
          payload: {
            token: resp.data.data.id,
            error: ''
          }
        })
        dispatch({
          type: ROUTING,
          payload: {
            nextUrl: '/profile'
        }})
      } else {
        dispatch({
          type: LOGIN_FAIL,
          payload: {
            error: LOGIN_FAIL_MESSAGE
          }
        })
      }
    } catch {
      dispatch({
        type: SERVER_ERROR,
        payload: SERVER_ERROR_MESASAGE
      })
    } 
    
  }
}

export const logout = () => {
  return (dispatch) => {
    token.logout()
    dispatch({
      type: LOGOUT
    })
  }
}
