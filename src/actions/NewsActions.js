import { NEWS_REQUEST, NEWS_SUCCESS } from "../constants/news";
import axios from 'axios';
import { host, url_news_get } from "../constants/urls";
import { SERVER_ERROR, SERVER_ERROR_MESASAGE } from "../constants/common";

export const getNews = () => {
  return async (dispatch) => {
    dispatch({
      type: NEWS_REQUEST
    })
    const resp = await axios.get(`${host}${url_news_get}`)
    try {
      dispatch({
        type: NEWS_SUCCESS,
        payload: resp.data.data
      })
    } catch {
      dispatch({
        type: SERVER_ERROR,
        payload: SERVER_ERROR_MESASAGE
      })
    }
    
    
  }
}